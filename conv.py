#! /usr/bin/env python3

import sys
import json
import csv
from typing import Iterable, Any


class DataPoint:
    """ Represents a datapoint inside of a mongo's metrics response """

    def __init__(self, timestamp: str, value: str) -> None:
        self.timestamp = timestamp
        self.value = value

    def __repr__(self) -> str:
        """ Returns a string representation of the DataPoint class """
        return "DataPoint { timestamp = %s, value = %s }" % (self.timestamp, self.value)


class Measurment:
    """ Represents a measuremnt inside of a mongo's metrics response """

    def __init__(self, name: str, unit: str, dataPoints: list[DataPoint]):
        self.name = name
        self.unit = unit
        self.dataPoints = dataPoints

    def __repr__(self) -> str:
        """ Returns a string representation of the Measurment class """
        return "Measurement { name = %s, unit = %s, dataPoints = %s }" % (self.name, self.unit, self.dataPoints)


class Metrics:
    """ Represents a metrics response from atlas mongo API """

    def __init__(self, hostId: str, measurements: list[Measurment]) -> None:
        self.hostId = hostId
        self.measurements = measurements

    def measurementNames(self) -> list[str]:
        """ Returns names of all available measurements """
        return [m.name for m in self.measurements]

    def timestamps(self) -> set[str]:
        """ Returns sorted set of all available timestamps """
        return sorted(set([d.timestamp for m in self.measurements for d in m.dataPoints]))

    def dataPointsByName(self, name: str) -> list[DataPoint]:
        """ Returns a list of datapoints for given name """
        return [m.dataPoints for m in self.measurements if m.name == name][0]

    def measureremntUnitByName(self, name: str) -> str:
        """ Returns measurment units for a measurement with given name """
        units = [m.unit for m in self.measurements if m.name == name]
        return units[0] if len(units) > 0 else None

    def __repr__(self) -> str:
        """ Returns a string representation of the Metrics class """
        return "Metric { hostId = %s, measurements = %s }" % (self.hostId, self.measurements)


def parse(filename: str) -> Metrics:
    """ 
    Parses an existing json file with the given name and returns the content as a Metrics instance
    """
    with open(filename) as file:
        data = json.load(file)
        measurements = []
        for measurement in data["measurements"]:
            dataPoints = [DataPoint(dataPoint['timestamp'], dataPoint['value'])
                          for dataPoint in measurement['dataPoints']]
            measurements.append(Measurment(
                measurement['name'], measurement['units'], dataPoints))
        return Metrics(data['hostId'], measurements)


def convertToMatrix(metrics: Metrics) -> Iterable[Iterable[str]]:
    """ Converts given metrics to a matrix (2d array) """
    header = ['Timestamp'] + metrics.measurementNames()
    timestamps = metrics.timestamps()
    # build our matrix filled with zeroes
    matrix = [[0] * len(header) for i in range(len(timestamps) + 1)]
    # set header row
    for i in range(len(header)):
        matrix[0][i] = header[i]
    # set first column (timestamps)
    for i in range(len(timestamps)):
        matrix[i+1][0] = timestamps[i]
    # set datapoints
    for r in range(1, len(timestamps)):
        for c in range(1, len(header)):
            name = matrix[0][c]
            timestamp = matrix[r][0]
            dataPoints = metrics.dataPointsByName(name)
            # find the value for the right timestamp
            values = [d.value for d in dataPoints if d.timestamp == timestamp]
            assert len(values) <= 1  # we do expect max 1 element
            if len(values) == 1 and values[0] is not None:
                matrix[r][c] = values[0]
            else:
                matrix[r][c] = 0
    # now extend measurements names for units so it looks like MEASUREMENT_NAME (MEASUREMENT_UNIT)
    for c in range(1, len(matrix[0])):
        # get units
        units = metrics.measureremntUnitByName(matrix[0][c])
        newName = "%s (%s)" % (matrix[0][c], units)
        matrix[0][c] = newName
    return matrix


def writeCsv(filename: str, matrix: Iterable[Iterable[Any]]) -> None:
    """ Writes given matrix to the file with given name """
    with open(filename, 'w') as f:
        csv.writer(f).writerows(matrix)


# entry point of this script
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("filename expected")
        sys.exit(1)

    # parse json file and extract the metrics
    metrics = parse(sys.argv[1])
    # convert the metrics to a matrix and put it to a csv file 
    writeCsv(
        metrics.hostId + '.csv',
        convertToMatrix(metrics)
    )
