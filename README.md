# fetch metrics

```shell
./fetch-metrics.sh [API-USER] [API_PASSWORD]
```

# convert metrics to csv

```
./conv.py example1.json
```
