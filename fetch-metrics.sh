#! /usr/bin/env bash

for n in 0 1 2
do
    SHARD=prod-shard-00-0${n}.etya8.azure.mongodb.net:27017
    echo fetching metrics for $SHARD ...
    http -A digest -a $1:$2 https://cloud.mongodb.com/api/atlas/v1.0/groups/5d5ce93aff7a259f5eb7a2db/processes/${SHARD}/measurements granularity==PT24H period==PT168H pretty==true > metrics-$((n+1)).json
done
